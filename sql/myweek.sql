-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `toggle` tinyint(1) DEFAULT 1,
  `default` tinyint(1) DEFAULT 0,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`categoryId`),
  KEY `categoriesIbfk2` (`userId`),
  CONSTRAINT `categoriesIbfk2` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `subtasks`;
CREATE TABLE `subtasks` (
  `subtaskId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `check` tinyint(1) DEFAULT 0,
  `taskId` int(11) NOT NULL,
  PRIMARY KEY (`subtaskId`),
  KEY `subtasksIbfk1` (`taskId`),
  CONSTRAINT `subtasksIbfk1` FOREIGN KEY (`taskId`) REFERENCES `tasks` (`taskId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `taskId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `progression` varchar(255) DEFAULT 'Pas Fait',
  `priority` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`taskId`),
  KEY `tasksIbfk1` (`categoryId`),
  KEY `tasksIbfk2` (`userId`),
  CONSTRAINT `tasksIbfk1` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`categoryId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tasksIbfk2` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `userConf`;
CREATE TABLE `userConf` (
  `configId` int(11) NOT NULL AUTO_INCREMENT,
  `theme` tinyint(1) NOT NULL,
  `showWeekend` tinyint(1) NOT NULL,
  `mailNotif` tinyint(1) NOT NULL,
  `appNotif` tinyint(1) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`configId`),
  KEY `userConfIbfk1` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2019-04-14 09:27:26

