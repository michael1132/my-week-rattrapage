<?php

namespace myweek\controller;

/* Slim */
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/* Auth */
use myweek\mf\auth\Authentification;

/* Models */
use myweek\model\Category;
use myweek\model\Task;
use myweek\model\User;

/* Errors / Response */
use myweek\response\Writter;
use myweek\errors\PhpError;
use myweek\errors\NotFound;

class CategoryController
{
    protected $app;

    public function __construct($pApp)
    {
        $this->app = $pApp;
    }

    /** Méthode index
     *
     * Récupère les categories de l'utilisateur avec les prioritées
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return NotFound|Response
     */
    public function index(Request $request, Response $response, array $args)
    {
        $token = Authentification::getToken($request);
        $user = User::find($token[1]->userId);
        if (!empty($user)) {
            $categories = Category::where("userId", "=", $user->userId)->get();
            $result['categories'] = [];
            foreach ($categories as $category) {
                $categoryData = [];
                $tasks = Task::where("categoryId", "=", $category->categoryId)->get();
                if (!empty($tasks)) {
                    $i = 0;
                    $k = 0;
                    $m = 0;
                    foreach ($tasks as $task) {
                        $categoryData = [];
                        switch ($task->priority) {
                            case 1:
                                $i++;
                                break;
                            case 2:
                                $k++;
                                break;
                            case 3:
                                $m++;
                                break;
                        }

                    }
                }
                $categoryData['category'] = $category;
                $categoryData['priorities']["prio1"] = $i;
                $categoryData['priorities']["prio2"] = $k;
                $categoryData['priorities']["prio3"] = $m;
                array_push($result['categories'], $categoryData);
            }
            return Writter::jsonSuccess($response, $result, 200, 'collection');
        } else {
            return Writter::jsonError($response, "Utilisateur inconnu", 404);
        }
    }

    /** Méthode create
     *
     * Ajoute une catégorie
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return PhpError|Response
     */
    public function create(Request $request, Response $response, array $args)
    {
        $body = $request->getParsedBody();
        $name = "";
        $color = "";
        $error = false;

        if (filter_var($body['name'], FILTER_SANITIZE_STRING)) {
            $name = $body['name'];

            if(empty($name)){
                $error = true;
            }
        } else {
            $error = true;
        }
        if (filter_var($body['color'], FILTER_SANITIZE_STRING)) {
            $color = $body['color'];

            if(empty($color)){
                $error = true;
            }
        } else {
            $error = true;
        }

        if (!$error) {
            try {
                $token = Authentification::getToken($request);
                $newCategory = new Category();
                $newCategory->name = $name;
                $newCategory->color = $color;
                $newCategory->userId = $token[1]->userId;
                $newCategory->default = 0;
                $newCategory->save();

                return Writter::jsonSuccess($response, array('success' => 1, 'categoryId' => $newCategory->categoryId), 201);
            } catch (\Exception $e) {
                return PhpError::error($request, $response, "Erreur dans la programmation");
            }
        } else {
            return Writter::jsonError($response, "Données manquantes", 403);
        }
    }

    /** Méthode edit
     *
     * Modifie en partie la catégorie (PATCH)
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function edit(Request $request, Response $response, array $args): Response
    {
        if (isset($args['id'])) {
            $id = $args['id'];
            $category = Category::find($id);

            if (!empty($category)) {
                $body = $request->getParsedBody();

                if (isset($body['name'])) {
                    $name = "";
                    if (filter_var($body['name'], FILTER_SANITIZE_STRING)) {
                        $name = $body['name'];
                    } else {
                        $error = true;
                    }
                    if (!$error) {
                        $category->name = $name;
                    } else {
                        return Writter::jsonError($response, "Nom incorrect", 403);
                    }
                } 
                if (isset($body['color'])) {
                    $color = "";
                    if (filter_var($body['color'], FILTER_SANITIZE_STRING)) {
                        $color = $body['color'];
                    } else {
                        $error = true;
                    }
                    if (!$error) {
                        $category->color = $color;
                    } else {
                        return Writter::jsonError($response, "Couleur incorrect", 403);
                    }
                } 
                if (isset($body['toggle'])) {
                    $category->toggle = $body['toggle'];
                } 

                try {
                    $category->save();
                    return Writter::jsonSuccess($response, array('success' => 1), 204);
                } catch (\Exception $e) {
                    PhpError::error($request, $response, "Erreur dans la programmation");
                }
            } else {
                return Writter::jsonError($response, "Categorie inconnue", 404);
            }
        } else {
            return Writter::jsonError($response, "Id est manquant", 403);
        }
    }

    /** Méthode delete
     *
     * Supprime une catégorie et ses taches
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function delete(Request $request, Response $response, array $args): Response
    {
        if (isset($args['id'])) {
            $id = $args['id'];
            $token = Authentification::getToken($request);
            $category = Category::where("categoryId", "=", $id)->where("userId", "=", $token[1]->userId)->first();

            if (!empty($category)) {
                try {
                    $category->delete();
                    return Writter::jsonSuccess($response, array('success' => 1), 204);
                } catch (\Exception $e) {
                    PhpError::error($request, $response, "Erreur dans la programmation");
                }
            } else {
                return Writter::jsonError($response, "La categorie est incunnue", 404);
            }

        } else {
            return Writter::jsonError($response, "Id est manquant", 403);
        }
    }
}