<?php

namespace myweek\controller;

/* Slim */
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/* JWT / Auth */
use Firebase\JWT\JWT;
use myweek\mf\auth\Authentification;

/* Models */
use myweek\model\Category;
use myweek\model\User;

/* Errors / Response */
use myweek\response\Writter;
use myweek\errors\PhpError;


class AccountController
{
    protected $app;

    public function __construct($pApp)
    {
        $this->app = $pApp;
    }

    /** Méthode edit
     * Modifie le profil d'un utilisateur
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     *
     */
    public function edit(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id'];
        $body = $req->getParsedBody();

        $body = self::checkProfileData($body, true);

        if($body) {
            $profile = User::find($id);
            if(!empty($profile)){
                try {
                    $profile->firstName = $body['firstName'];
                    $profile->lastName = $body['lastName'];
                    $profile->username = $body['username'];
                    $profile->email = $body['email'];
                    if ($body['password'] !== '') {
                        $profile->password = Authentification::hashPassword($body['password']);
                    }
    
                    $profile->save();
                    // pour ne pas renvoyer le mot de passe dans la réponse
                    unset($profile->password);
    
                    $jwt = JWT::encode($profile, $profile->token, 'HS512');
                    // pour ne pas renvoyer le token secret dans la réponse
                    unset($profile->token);
                    $result = [
                        'token' => $jwt,
                        'user' => $profile
                    ];
                    return Writter::jsonSuccess($resp, $result);
                } catch (\Exception $e) {
                    return PhpError::error($request, $response, "Erreur dans la programmation");
                }
            }
            else{
                return Writter::jsonError($resp, "Profil non trouvé", 404);
            }
        } else {
            return Writter::jsonError($resp, "Erreur dans les données", 403);
        }
    }

    /** Méthode checkProfileData
     *
     * Vérifie si les données de profil ne sont pas nulles et les assainit (sanitize)
     *
     * @param array $body
     * @param bool $isUpdate true si on est en modification, par defaut false en création
     * @return bool|array les données profil propres, false si erreur
     */
    private static function checkProfileData($body, $isUpdate = false)
    {
        $error = false;

        if (empty($body['firstName']) || !filter_var($body['firstName'], FILTER_SANITIZE_STRING)) {
            $error = true;
        }
        if (empty($body['lastName']) || !filter_var($body['lastName'], FILTER_SANITIZE_STRING)) {
            $error = true;
        }
        if (empty($body['username']) || !filter_var($body['username'], FILTER_SANITIZE_STRING)) {
            $error = true;
        }
        if (!filter_var($body['email'], FILTER_VALIDATE_EMAIL)) {
            $error = true;
        }
        if (!$isUpdate || ($isUpdate && !empty($body['password']))) {
            if (empty($body['password']) || !filter_var($body['password'], FILTER_SANITIZE_STRING)) {
                $error = true;
            }
            if (empty($body['verifPassword']) || !filter_var($body['verifPassword'], FILTER_SANITIZE_STRING)) {
                $error = true;
            }
        }

        if ($error == true) {
            return false;
        }

        return $body;
    }

    /** Méthode getProfil
     *
     * Récupère les informations de profil d'un utilisateur
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function index(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id'];
        try {
            $profil = User::findOrFail($id);
            $result["profil"] = $profil;
            return Writter::jsonsuccess($resp, $result);

        } catch (\Exception $e) {
            return Writter::jsonError($resp, "Profil non trouvé", 404);
        }
    }

    /** Méthode create
     * Créé un compte
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function create(Request $req, Response $resp, array $args): Response
    {
        $body = $req->getParsedBody();
        $body = self::checkProfileData($body);
        //Si il n'y a pas d'erreur dans la verification et sanitisation des données
        if ($body) {
            if ($body['password'] == $body['verifPassword']) {
                $user = User::where('email', '=', $body['email'])->first();

            //if($body['password'] == $body['verifPassword']){
                $user = User::where('email','=',$body['email'])->first();
    
                if(empty($user)){
                    try{
                        $password = Authentification::hashPassword($body['password']);

                        $newUser = new User();
                        $newUser->firstName = $body['firstName'];
                        $newUser->lastName = $body['lastName'];
                        $newUser->username = $body['username'];
                        $newUser->email = $body['email'];
                        $newUser->password = $password;
                        $newUser->save();

                        $defaultCategory = new Category();
                        $defaultCategory->name = "Mes tâches";
                        $defaultCategory->color = "blue";
                        $defaultCategory->toggle = 1;
                        $defaultCategory->default = 1;

                        $defaultCategory->user()->associate($newUser)->save();

                        unset($newUser->password);

                        return Writter::jsonSuccess($resp, array('user' => $newUser), 201);
                    } catch (\Exception $e) {
                        return PhpError::error($req, $resp, "Erreur dans la programmation");
                    }
                } else {
                    return Writter::jsonError($resp, "Le compte existe déjà", 409);
                }
            } else {
                return Writter::jsonError($resp, "Les mots de passe ne correspondent pas", 409);
            }
        } else {
            return Writter::jsonError($resp, "Erreur dans les données", 403);
        }
    }

    /** Méthode login
     * Donne un token à l'utilisateur donné dans le body
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return PhpError|Response
     */
    public function login(Request $req, Response $resp, array $args)
    {
        $body = $req->getParsedBody();

        $email = "";
        $password = "";
        $error = false;

        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
            return Writter::jsonError($resp, 'Identifiants requis', 401);
        }

        $email = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];
        try {
            $user = User::where("email", "=", $email)->first();

            if (!empty($user)) {

                if (Authentification::verifyPassword($password, $user->password)) {
                    do {
                        $key = random_bytes(32);
                        $key = bin2hex($key);
                        $keyUser = User::where('token', '=', $key)->first();
                    } while (!empty($keyUser));
                    $user->token = $key;
                    $user->save();

                    unset($user->password);

                    $jwt = JWT::encode($user, $key, 'HS512');
                    $result['token'] = $jwt;
                    $result['iss'] = "http://api.backoffice.local";
                    $result['aud'] = "http://api.backoffice.local";
                    $result['iat'] = time();
                    $result['exp'] = time() + 36000;
                    return Writter::jsonSuccess($resp, $result);

                } else {
                    return Writter::jsonError($resp, "Identifiants incorrects", 401);
                }
            } else {
                return Writter::jsonError($resp, "Profil non trouvé", 404);
            }
        } catch (\Exception $e) {
            return PhpError::error($req, $resp, "Erreur dans la programmation");
        }
    }

    /** Méthode logout
     * Supprime le token de l'utilisateur
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function logout(Request $req, Response $resp, array $args): Response
    {
        $token = Authentification::getToken($req);
        $user = User::find($token[1]->userId);
        if (!empty($user)) {
            try {
                $user->token = "";
                $user->save();
                $result['success'] = 1;
                return Writter::jsonSuccess($resp, $result);
            } catch (\Exception $e) {
                return PhpError::error($req, $resp, "Erreur dans la programmation");
            }
        } else {
            return Writter::jsonError($resp, "Utilisateur inconnu", 404);
        }
    }

    /** Méthode search
     * Liste des utilisateurs selon le contenu de la variable search dans l'uri
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return PhpError|Response
      */
    public function search(Request $req, Response $resp, array $args): Response
    {
        $params = $req->getQueryParams();
        $error = false;
        if(isset($params['search'])){
            $search = filter_var($params['search'], FILTER_SANITIZE_STRING);
        }
        else{
            $error = true;
        }

        if(!$error){
            try{
                $users = User::where('firstName','LIKE', '%'.$search.'%')->orWhere('lastName','LIKE', '%'.$search.'%')->get();
                foreach ($users as $user) {
                    $user['fullName'] = ucfirst($user->firstName)." ".ucfirst($user->lastName);
                    unset($user->password);
                    unset($user->token);
                    unset($user->created_at);
                    unset($user->updated_at);
                    unset($user->email);
                }
                return Writter::jsonSuccess($resp, array('users' => $users), 200, 'collection');
            }
            catch(\Exception $e){
                return PhpError::error($req, $resp, "Erreur dans la programmation");
            }
        }
        else{
            return Writter::jsonError($resp, "Paramètres manquants", 403);
        }
    }

    /** Méthode edit
     * Modifie le postit d'un utilisateur
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     *
     */
    public function savePostits(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id'];
        $body = $req->getParsedBody();
        
        $postit = User::find($id);
        if(!empty($postit)){
            try {
                $postit->content = filter_var($body['content'], FILTER_SANITIZE_STRING);

                $postit->save();
                
                $result = [
                    'postit' => $postit
                ];
                return Writter::jsonSuccess($resp, $result);
            } catch (\Exception $e) {
                return PhpError::error($request, $response, "Erreur dans la programmation");
            }
        }
        else{
            return Writter::jsonError($resp, "Profil non trouvé", 404);
        }
    }

    /** Méthode getPostits
     *
     * Récupère les informations du postit d'un utilisateur
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function getPostits(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id'];
        try {
            $postit = User::findOrFail($id);
            $result["postit"] = $postit;
            return Writter::jsonsuccess($resp, $result);

        } catch (\Exception $e) {
            return Writter::jsonError($resp, "Profil non trouvé", 404);
        }
    }
}