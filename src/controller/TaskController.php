<?php

namespace myweek\controller;
/* Slim */
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/* Auth */
use myweek\mf\auth\Authentification;

/* Models */
use myweek\model\Category;
use myweek\model\Subtask;
use myweek\model\Task;
use myweek\model\User;

/* Errors / Response */
use myweek\response\Writter;
use myweek\errors\PhpError;

class TaskController
{
    protected $app;

    public function __construct($pApp)
    {
        $this->app = $pApp;
    }

    /** Méthode index
     * Récupère les taches de l'utilisateur
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return PhpError|Response
     */
    public function index(Request $request, Response $response, array $args)
    {
        $token = Authentification::getToken($request);
        try{
            $tasks = Task::where("userId","=",$token[1]->userId)
                        ->orderBy('startDate', 'ASC')
                        ->orderBy('endDate', 'DESC')
                        ->get();
            foreach ($tasks as $task) {
                $task->category;
                $task->subtasks;
            }
            $data['tasks'] = $tasks;
            return Writter::jsonSuccess($response, $data, 200, 'collection');
        } catch (\Exception $e) {
            return PhpError::error($request, $response, "Erreur dans la programmation");
        }
    }

    /** Méthode create
     * Ajoute une tache à l'agenda de l'utilisateur
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return PhpError|Response
     */
    public function create(Request $request, Response $response, array $args)
    {
        $body = $request->getParsedBody();
        $description = null;
        $subtasks = false;
        $usersId = false;
        $error = false;

        if (isset($body['title'])) {
            $title = filter_var($body['title'], FILTER_SANITIZE_STRING);

            if(empty($title)){
                $error = true;
            }
        } else {
            $error = true;
        }
        if (isset($body['description'])) {
            $description = filter_var($body['description'], FILTER_SANITIZE_STRING);
        }
        if (isset($body['categoryId'])) {
            $categoryId = filter_var($body['categoryId'], FILTER_SANITIZE_NUMBER_INT);

            if(empty($categoryId)){
                $error = true;
            }
        } else {
            $error = true;
        }
        if (isset($body['priority'])) {
            $priority = filter_var($body['priority'], FILTER_SANITIZE_NUMBER_INT);

            if(empty($priority)){
                $error = true;
            }
        } else {
            $error = true;
        }
        if (isset($body['startDate'])) {
            $startDate = filter_var($body['startDate'], FILTER_SANITIZE_STRING);
            
            if(empty($startDate)){
                $error = true;
            }
        } else {
            $error = true;
        }
        if (isset($body['endDate'])) {
            $endDate = filter_var($body['endDate'], FILTER_SANITIZE_STRING);

            if(empty($endDate)){
                $error = true;
            }
        } else {
            $error = true;
        }
        if(isset($body['subtasks'])){
            $subtasks = $body['subtasks'];
        }
        if(isset($body['usersId'])){
            $usersId = $body['usersId'];
        }
        if (isset($body['endDateFrequence'])) {
            $endDateFrequence = filter_var($body['endDateFrequence'], FILTER_SANITIZE_STRING);

            if(empty($endDateFrequence)){
                $error = true;
            }
        } else {
            $error = true;
        }
        if (isset($body['frequence'])) {
            $frequence = filter_var($body['frequence'], FILTER_SANITIZE_STRING);

            if(empty($frequence)){
                $error = true;
            }
        } else {
            $error = true;
        }
        if (!$error) {
            try {
                
                while(strtotime($endDate) <= strtotime($endDateFrequence)) {
                    
                    $token = Authentification::getToken($request);
                    $newTask = new Task();
                    $newTask->title = $title;
                    $newTask->description = $description;
                    $newTask->startDate = $startDate;
                    $newTask->endDate = $endDate;
                    $newTask->priority = $priority;
                    $newTask->categoryId = $categoryId;
                    $newTask->userId = $token[1]->userId;
                    $newTask->save();
                    
                    if ($subtasks) {
                        foreach ($subtasks as $sub) {
                            $newSubtask = new Subtask();
                            $newSubtask->title = $sub;
                            $newTask->subtasks()->save($newSubtask);
                        }
                    }

                    if ($frequence == 1) {
                        $startDate = date('Y-m-d', strtotime($startDate . "+1 days"));
                        $endDate = date('Y-m-d', strtotime($endDate . "+1 days"));
                    }
                    if ($frequence == 2) {
                        $startDate = date('Y-m-d', strtotime($startDate . "+7 days"));
                        $endDate = date('Y-m-d', strtotime($endDate . "+7 days"));
                    }
                    if ($frequence == 3) {
                        $startDate = date('Y-m-d', strtotime($startDate . "+1 months"));
                        $endDate = date('Y-m-d', strtotime($endDate . "+1 months"));
                    }
                } 

                if($usersId){
                    foreach ($usersId as $userId) {
                        $this->shareTask($response, $newTask, $userId, $subtasks, $frequence, $endDateFrequence);
                    }
                }

                return Writter::jsonSuccess($response, array('success' => 1), 204);
            } catch (\Exception $e) {
                return PhpError::error($request, $response, "Erreur dans la programmation");
            }
        } else {
            return Writter::jsonError($response, "Données manquantes", 403);
        }
    }

    /** Méthode edit
     * Modifie en partie une tache (PATCH)
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function edit(Request $request, Response $response, array $args): Response
    {
        if (isset($args['id'])) {
            $id = $args['id'];
            $token = Authentification::getToken($request);
            $task = Task::where("taskId", "=", $id)->where("userId", "=", $token[1]->userId)->first();
            $error = false;

            if (!empty($task)) {
                $body = $request->getParsedBody();

                if (isset($body['title'])) {
                    if(empty($body['title'])){
                        $error = true;
                    }
                    else{
                        $task->title = filter_var($body['title'], FILTER_SANITIZE_STRING);
                    }
                }
                if (isset($body['description'])) {
                    $task->description = filter_var($body['description'], FILTER_SANITIZE_STRING);
                }
                if (isset($body['startDate'])) {
                    if(empty($body['startDate'])){
                        $error = true;
                    }
                    else{
                        $task->startDate = filter_var($body['startDate'], FILTER_SANITIZE_STRING);
                    }
                }
                if (isset($body['endDate'])) {
                    if(empty($body['endDate'])){
                        $error = true;
                    }
                    else{
                        $task->endDate = filter_var($body['endDate'], FILTER_SANITIZE_STRING);
                    }
                }
                if (isset($body['progression'])) {
                    if(empty($body['progression'])){
                        $error = true;
                    }
                    else{
                        $task->progression = filter_var($body['progression'], FILTER_SANITIZE_STRING);
                    }
                }
                if (isset($body['priority'])) {
                    if(empty($body['priority'])){
                        $error = true;
                    }
                    else{
                        $task->priority = filter_var($body['priority'], FILTER_SANITIZE_NUMBER_INT);
                    }
                }
                if (isset($body['categoryId'])) {
                    if(empty($body['categoryId'])){
                        $error = true;
                    }
                    else{
                        $task->categoryId = filter_var($body['categoryId'], FILTER_SANITIZE_NUMBER_INT);
                    }
                }
                if(!$error){
                    try {
                        $task->save();

                        $subtasks = [];

                        foreach ($task->subtasks as $subtask) {
                            array_push($subtasks, $subtask->title);
                        }


                        if(!empty($body['usersId'])){
                            $usersId = $body['usersId'];

                            foreach ($usersId as $userId) {
                                $this->shareTask($response, $task, $userId, $subtasks);
                            }

                        }

                        return Writter::jsonSuccess($response, array('success' => $task->taskId), 201);
                    } catch (\Exception $e) {
                        PhpError::error($request, $response, "Erreur dans la programmation");
                    }
                } else {
                    return Writter::jsonError($response, "Données manquantes", 403);
                }
            } else {
                return Writter::jsonError($response, "Tache inconnue", 404);
            }
        } else {
            return Writter::jsonError($response, "ID est manquant", 403);
        }
    }

    /** Méthode delete
     * Supprime une tache dans l'agenda de l'utilisateur
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return PhpError|Response
     */
    public function delete(Request $request, Response $response, array $args)
    {
        if (isset($args['id'])) {
            $id = $args['id'];
            $token = Authentification::getToken($request);
            $task = Task::where("taskId", "=", $id)->where("userId", "=", $token[1]->userId)->first();

            if (!empty($task)) {
                try {
                    $task->delete();
                    return Writter::jsonSuccess($response, array('success' => 1), 204);
                } catch (\Exception $e) {
                    return PhpError::error($request, $response, "Erreur dans la programmation");
                }
            } else {
                return Writter::jsonError($response, "La tâche est incunnue", 404);
            }

        } else {
            return Writter::jsonError($response, "Id est manquant", 403);
        }
    }

    /** Methode shareTask
     * Crée un tache pour un autre utilisateur
     * 
     * @param Response $response
     * @param Task $task
     * @param int $userId
     * @param array $subtasks
     * @return void|Response
      */
    private function shareTask(Response $response, Task $task, int $userId, $subtasks, $frequence, $endDatefrequence)
    {
        $user = User::find($userId);

        if(!empty($user)){
            $defaultCategory = Category::where('userId','=',$userId)->where('default','=',1)->first();

            if(!empty($defaultCategory)){
                try{

                    $startDate = $task->startDate;
                    $endDate = $task->endDate;

                    while(strtotime($endDate) <= strtotime($endDateFrequence)) {

                        $newTask = new Task();
                        $newTask->title = $task->title;
                        $newTask->description = $task->description;
                        $newTask->startDate = $startDate;
                        $newTask->endDate = $endDate;
                        $newTask->priority = $task->priority;
                        $newTask->categoryId = $defaultCategory->categoryId;
                        $newTask->userId = $userId;
                        $newTask->save();

                        if ($subtasks) {
                            foreach ($subtasks as $sub) {
                                $newSubTask = new Subtask();
                                $newSubTask->title = $sub;
                                $newTask->subtasks()->save($newSubTask);
                            }
                        }

                        if ($frequence == 1) {
                            $startDate = date('Y-m-d', strtotime($startDate . "+1 days"));
                            $endDate = date('Y-m-d', strtotime($endDate . "+1 days"));
                        }
                        if ($frequence == 2) {
                            $startDate = date('Y-m-d', strtotime($startDate . "+7 days"));
                            $endDate = date('Y-m-d', strtotime($endDate . "+7 days"));
                        }
                        if ($frequence == 3) {
                            $startDate = date('Y-m-d', strtotime($startDate . "+1 months"));
                            $endDate = date('Y-m-d', strtotime($endDate . "+1 months"));
                        }
                    } 

                } catch (\Exception $e) {
                    return PhpError::error($request, $response, "Erreur dans la programmation");
                }
            }
            else{
                return Writter::jsonError($response, "Aucune categorie par defaut pour l'utilisateur ".$user->firstName, 403);
            }
        }
        else{
            return Writter::jsonError($response, "Utilisateur inconnu", 404);
        }
    }
}