<?php

namespace myweek\eloquent;

class Eloquent{

    /** Méthode startEloquent
     * Fait une connection eloquant
     *
     * @param  mixed $config
     *
     * @return void
     */
    public static function startEloquent($config){
        $db = new \Illuminate\Database\Capsule\Manager();

        $db->addConnection( parse_ini_file($config ));
        $db->setAsGlobal();
        $db->bootEloquent();
    }
}
