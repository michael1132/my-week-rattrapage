<?php

namespace myweek\errors;

/* Slim */
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/* Error / Response */
use myweek\response\Writter;

class PhpError{

    /** Méthode error
     * Revoit une erreur serveur 500
     *
     * @param  mixed $rq
     * @param  mixed $rs
     * @param  mixed $error
     *
     * @return Response
     */
    public static function error(Request $rq, Response $rs, $error){
            return Writter::jsonError($rs, $error, 500);
    }

}