<?php

namespace myweek\errors;

/* Slim */
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class NotAllowed{

    /** Méthode error
     * Renvoit une erreur 405
     *
     * @param  mixed $rq
     * @param  mixed $rs
     * @param  mixed $methods
     *
     * @return Response
     */
    public static function error(Request $rq, Response $rs, $methods){
            $method = $rq->getMethod();
            $uri = $rq->getUri();
            $result['type'] = "error";
            $result['error'] = 405;
            $result['message'] = "Method not allowed $method in $uri";
            $resp = $rs
                ->withHeader('Allow',$methods)
                ->withHeader('Content-Type', 'application/json;charset=utf-8')
                ->withStatus(405);
            $resp->getBody()->write(json_encode($result));
            return $resp;
    }

}