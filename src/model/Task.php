<?php

namespace myweek\model;

class Task extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'tasks';
    protected $primaryKey = 'taskId';
    public $timestamps = false;

    public function category(){
        return $this->belongsTo('myweek\model\Category', 'categoryId');
    }

    public function subtasks(){
        return $this->hasMany('myweek\model\Subtask', 'taskId');
    }
}