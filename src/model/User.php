<?php

namespace myweek\model;

class User extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'users';
    protected $primaryKey = 'userId';
    public $timestamps = true;

    public function categories(){
        return $this->hasMany('myweek\model\Category','userId','userId');
    }

    public function config(){
        return $this->belongsTo('myweek\model\UserConf', 'userId');
    }

    public function tasks(){
        return $this->hasMany('myweek\model\Task', 'userId');
    }
}