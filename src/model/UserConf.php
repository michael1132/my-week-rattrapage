<?php

namespace myweek\model;

class UserConf extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'userConf';
    protected $primaryKey = 'configId';
    public $timestamps = false;
}