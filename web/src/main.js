/* eslint-disable */
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import { Utils } from './components/mixins/utils.js'
import moment from 'moment';
import "./styles/global.scss";
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';


/* Init Moment */
moment.locale('fr');

Vue.prototype.moment = moment;

Vue.mixin(Utils);

Vue.config.productionTip = false;

Vue.prototype.$bus = new Vue();

/* Retrieved localStorage */
store.subscribe((mutation, state) => {
  localStorage.setItem('store', JSON.stringify(state))
});

/* Init Axios */
window.axios = axios.create({
    // baseURL: 'https://apimobilegeoquizz.pagekite.me/',
    baseURL: 'http://api.myweek.local:3030',
    headers: {
      Authorization: false,
    }
});

Vue.use(Vuetify, {
  theme: {
      primary: '#4FC3F7',
      secondary: '#204177',
      accent: '#43A047',
      error: '#F44336',
      warning: '#9c27b0',
      info: '#ffc107',
      success: '#4caf50'
  }
});

new Vue({
  router,
  store,
  render: h => h(App),
  beforeCreate () {
    this.$store.commit('initialiseStore')
  }
}).$mount('#app');
