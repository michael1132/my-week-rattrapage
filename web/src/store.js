/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({

    state: {
        dayNameMini:["LUN.", "MAR.", "MER.", "JEU.", "VEN.", "SAM.", "DIM.",],
        viewChoice:'Mois',
        displayedPeriod: '',
        referenceDatePeriod: null, // jour de reference de la periode affichée (initialisé à aujourd'hui)
        toggleLeftAside : true,
        token: false,
        mainFullScreen: false,
        monthWith6Lines: false,
        daysList: [],
        positionsList: [],
        error: {
            is : false,
            msg : ""
        },
        success: {
            is : false,
            msg : ""
        },
        loading: false
    },

    mutations: {
        setLeftAside(state, toggle){
            state.toggleLeftAside = toggle;
        },
        setViewChoice(state, viewChoice) {
            state.viewChoice = viewChoice;
        },
        setDisplayedPeriod(state, displayedPeriod) {
            state.displayedPeriod = displayedPeriod;
        },

        setReferenceDatePeriod(state, referenceDatePeriod) {
            state.referenceDatePeriod = referenceDatePeriod;
        },

        setDaysList(state, daysList) {
            state.daysList = daysList;
        },

        setPositionsList(state, positionsList) {
            state.positionsList = positionsList;
        },

        toggleMainFullScreen(state, mainFullScreen){
            state.mainFullScreen = mainFullScreen;
        },

        toggleMonthWith6Lines(state, monthWith6Lines){
            state.monthWith6Lines = monthWith6Lines;
        },
        setToken(state, token){
            state.token = token;
        },

        setError(state, toggle){
            state.error.is = toggle;
        },
        setErrorMsg(state, msg){
            state.error.msg = msg;
        },

        setSuccess(state, toggle){
            state.success.is = toggle;
        },
        setSuccessMsg(state, msg){
            state.success.msg = msg;
        },

        setLoading(state, toggle){
            state.loading = toggle;
        },

        initialiseStore (state) {
            if (localStorage.getItem('store')) {
                this.replaceState(
                    Object.assign(state, JSON.parse(localStorage.getItem('store')))
                )
            }
        }
    },
    actions: {

    }
})
