/* eslint-disable */
import FileSaver from 'file-saver';

export const Utils = {
    methods: {

        // retourne le mois courant en toute lettre
        getCurrentMonthName() {
            return this.moment().format("MMMM");
        },

        // retourne l'année courante en toute lettre
        getCurrentYear() {
            return this.getCurrentDate().getFullYear();

        },

        // Retourne la période affichée
        getDisplayePeriod() {
            let date = this.$store.state.referenceDatePeriod;
            switch (this.$store.state.viewChoice) {
                case "Month":
                        return date.format('MMMM Y');
                case "Week":
                    return date.startOf('week').format('DD/MM/Y') + '- ' + date.endOf('week').format('DD/MM/Y');
                // case "Day":
                //     return 'todo';
                case "List":
                    return date.format('MMMM Y');
                case "Year":
                    return 'todo';
                default:
                    return 'todo default';
            }
        },

        // Met à jour la date de référence de la période affichée
        updateReferenceDate(newDate) {
            this.$store.commit('setPositionsList',[]);
            this.$store.commit('setReferenceDatePeriod', newDate);
            this.$store.commit('setDisplayedPeriod', this.getDisplayePeriod());
            this.$store.commit('setDaysList', this.getAllDaysInPeriod(newDate));
            if (this.$store.state.daysList.length > 35) {
                this.$store.commit('toggleMonthWith6Lines', true);

            } else {
                this.$store.commit('toggleMonthWith6Lines', false);
            }

        },

        getAllDaysInPeriod(reference) {
            switch (this.$store.state.viewChoice) {
                case "Month":
                    return this.getAllDaysInMonth(reference);
                case "Week":
                    return this.getAllDaysInWeek(reference);
                case "List":
                    return this.getAllDaysInMonth(reference);
                case "Year":
                    return [];
                default:
                    return [];
            }
        },


        // Récupère la totalité des jours à afficher dans la vue Mois
        getAllDaysInMonth(reference) {
            const referenceDay = this.moment(reference).format("DD");
            let startOfMonth = this.moment(reference).startOf('month');
            let firstCalendarDay = this.moment(reference).startOf('month');
            let listOfDays = [];
            if (startOfMonth.day() === 1) {
                //Le premier jour du mois est un lundi, ça veut dire que tout les jours
                //de la premiere ligne sont du 'currentPeriod'
                //On push pas, pour pas avoir les 2 '1er'
            } else { // Sinon il y a des jours avant qui son pas du 'currentPeriod'
                //firstCalendarDay.startOf('Week');
                firstCalendarDay.startOf('week');

                listOfDays.push(
                    {
                        'date': firstCalendarDay.toDate(),
                        'currentPeriod': false,
                        'isReferenceDate': false
                    }
                );
                let nextDay = this.moment(firstCalendarDay);
                nextDay.add(1, 'days');

                while (nextDay.isBefore(startOfMonth)) {
                    listOfDays.push(
                        {
                            'date': nextDay.toDate(),
                            'currentPeriod': false,
                            'isReferenceDate': false
                        }
                    );
                    nextDay.add(1, 'days');
                }
            } //Fin else

            // parcours des jours du mois
            let tmpDay = this.moment(startOfMonth);
            while (tmpDay.isSame(startOfMonth, 'month')) {
                listOfDays.push(
                    {
                        'date': tmpDay.toDate(),
                        'currentPeriod': true,
                        'isReferenceDate': (tmpDay.format('DD') === referenceDay)
                    }
                );
                tmpDay.add(1, 'days');
            }

            // tmpDay = premier jour du mois suivant
            // si le premier jour du mois suivant est un dimanche, on l'ajoute et c'est fini
            if (tmpDay.day() === 0) {
                listOfDays.push(
                    {
                        'date': tmpDay.toDate(),
                        'currentPeriod': false,
                        'isReferenceDate': false
                    }
                );
            } else if (tmpDay.day() !== 1) { // si c'est lundi on ne fait rien sinon on va jusqu'a la fin de semaine
                let dayNb = tmpDay.day();

                while (dayNb <= 7) {
                    listOfDays.push(
                        {
                            'date': tmpDay.toDate(),
                            'currentPeriod': false,
                            'isReferenceDate': false
                        }
                    );
                    tmpDay.add(1, 'days');
                    dayNb += 1;
                }
            }

            if ((listOfDays.length % 7) > 0) {
                listOfDays.pop();
            }

            return listOfDays;
        },

        getAllDaysInWeek(reference) {
            const referenceDay = this.moment(reference).format("DD");
            let tmpDay = this.moment(reference).startOf('week');
            let listOfDays = [];
            let dayNb = tmpDay.day();

            while (dayNb <= 7) {
                listOfDays.push(
                    {
                        'date': tmpDay.toDate(),
                        'currentPeriod': true,
                        'isReferenceDate': tmpDay.format("DD") === referenceDay
                    }
                );
                tmpDay.add(1, 'days');
                dayNb += 1;
            }
            return listOfDays;

        },

        setToken(token){
            axios.defaults.headers.Authorization = "Bearer "+token;
        },

        memberConnected(){
            if(this.$store.state.token === false){
                return false;
            }
            else{
                this.setToken(this.$store.state.token);
                return true;
            }
        },

        parseJwt(token) {
            try {
                // Récupérartion du Token Header
                const base64HeaderUrl = token.split('.')[0];
                const base64Header = base64HeaderUrl.replace('-', '+').replace('_', '/');
                const headerData = JSON.parse(window.atob(base64Header));

                // Récupération du Token payload et date
                const base64Url = token.split('.')[1];
                const base64 = base64Url.replace('-', '+').replace('_', '/');
                const dataJWT = JSON.parse(window.atob(base64));
                dataJWT.header = headerData;

            // TODO: add expiration at check ...

                return dataJWT;
            } catch (err) {
                return false;
            }
        },

        getJwtData()
        {
            // JWT's are two base64-encoded JSON objects and a trailing signature
            // joined by periods. The middle section is the data payload.
            if (this.$store.state.token) return JSON.parse(atob(this.jwt.split('.')[1]));
            return {};
        },

        // permet de trier un tableau de taches selon les dates de début et la durée
        sortByStartAndDuration(task1,task2) { // trie selon la durée
            const date1 = this.moment(task1.startDate);
            const date2 = this.moment(task2.startDate);

            if (date1.isAfter(date2)) return 1;
            if (date2.isAfter(date1)) return -1;

            const duration1 = this.moment(task1.startDate).diff(this.moment(task1.endDate));
            const duration2 = this.moment(task2.startDate).diff(this.moment(task2.endDate));
            if (duration1 > duration2) return 1;
            if (duration2 > duration1) return -1;
            return 0;
        },

        getMinPosition(tab) {
            let min = 0;
            tab.forEach( pos => {
                if( pos === min) {
                    min++;
                }
            });
            return min;
        },

        updateTasks(){
            this.$bus.$emit('updTasks');
        },

        updateCategories(){
            this.$bus.$emit('updCategories');
        },

        updateFuturTasks(){
            this.$bus.$emit('updFuturTasks');
        },

        ics(uidDomain, prodId){ //ICS == format de iCalendar ()
          'use strict';

          if (navigator.userAgent.indexOf('MSIE') > -1 && navigator.userAgent.indexOf('MSIE 10') == -1) {
            return;
          }

          if (typeof uidDomain === 'undefined') { uidDomain = 'default'; }
          if (typeof prodId === 'undefined') { prodId = 'Calendar'; }

          var SEPARATOR = (navigator.appVersion.indexOf('Win') !== -1) ? '\r\n' : '\n';
          var calendarEvents = [];
          var calendarStart = [
            'BEGIN:VCALENDAR',
            'PRODID:' + prodId,
            'VERSION:2.0'
          ].join(SEPARATOR);
          var calendarEnd = SEPARATOR + 'END:VCALENDAR';
          var BYDAY_VALUES = ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'];

          return {
            /**
             * Returns events array
             * @return {array} Events
             */
            'events': function() {
              return calendarEvents;
            },

            /**
             * Returns calendar
             * @return {string} Calendar in iCalendar format
             */
            'calendar': function() {
              return calendarStart + SEPARATOR + calendarEvents.join(SEPARATOR) + calendarEnd;
            },

            /**
             * Add event to the calendar
             * @param  {string} subject     Subject/Title of event
             * @param  {string} description Description of event
             * @param  {string} location    Location of event
             * @param  {string} begin       Beginning date of event
             * @param  {string} stop        Ending date of event
             */
            'addEvent': function(subject, description, location, begin, stop, rrule) {
              // I'm not in the mood to make these optional... So they are all required
              if (typeof subject === 'undefined' ||
                typeof description === 'undefined' ||
                typeof location === 'undefined' ||
                typeof begin === 'undefined' ||
                typeof stop === 'undefined'
              ) {
                return false;
              }

              // validate rrule
              if (rrule) {
                if (!rrule.rrule) {
                  if (rrule.freq !== 'YEARLY' && rrule.freq !== 'MONTHLY' && rrule.freq !== 'WEEKLY' && rrule.freq !== 'DAILY') {
                    throw "Recurrence rrule frequency must be provided and be one of the following: 'YEARLY', 'MONTHLY', 'WEEKLY', or 'DAILY'";
                  }

                  if (rrule.until) {
                    if (isNaN(Date.parse(rrule.until))) {
                      throw "Recurrence rrule 'until' must be a valid date string";
                    }
                  }

                  if (rrule.interval) {
                    if (isNaN(parseInt(rrule.interval))) {
                      throw "Recurrence rrule 'interval' must be an integer";
                    }
                  }

                  if (rrule.count) {
                    if (isNaN(parseInt(rrule.count))) {
                      throw "Recurrence rrule 'count' must be an integer";
                    }
                  }

                  if (typeof rrule.byday !== 'undefined') {
                    if ((Object.prototype.toString.call(rrule.byday) !== '[object Array]')) {
                      throw "Recurrence rrule 'byday' must be an array";
                    }

                    if (rrule.byday.length > 7) {
                      throw "Recurrence rrule 'byday' array must not be longer than the 7 days in a week";
                    }

                    // Filter any possible repeats
                    rrule.byday = rrule.byday.filter(function(elem, pos) {
                      return rrule.byday.indexOf(elem) == pos;
                    });

                    for (var d in rrule.byday) {
                      if (BYDAY_VALUES.indexOf(rrule.byday[d]) < 0) {
                        throw "Recurrence rrule 'byday' values must include only the following: 'SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'";
                      }
                    }
                  }
                }
              }

              //TODO add time and time zone? use moment to format?
              var start_date = new Date(begin);
              var end_date = new Date(stop);
              var now_date = new Date();

              var start_year = ("0000" + (start_date.getFullYear().toString())).slice(-4);
              var start_month = ("00" + ((start_date.getMonth() + 1).toString())).slice(-2);
              var start_day = ("00" + ((start_date.getDate()).toString())).slice(-2);
              var start_hours = ("00" + (start_date.getHours().toString())).slice(-2);
              var start_minutes = ("00" + (start_date.getMinutes().toString())).slice(-2);
              var start_seconds = ("00" + (start_date.getSeconds().toString())).slice(-2);

              var end_year = ("0000" + (end_date.getFullYear().toString())).slice(-4);
              var end_month = ("00" + ((end_date.getMonth() + 1).toString())).slice(-2);
              var end_day = ("00" + ((end_date.getDate()).toString())).slice(-2);
              var end_hours = ("00" + (end_date.getHours().toString())).slice(-2);
              var end_minutes = ("00" + (end_date.getMinutes().toString())).slice(-2);
              var end_seconds = ("00" + (end_date.getSeconds().toString())).slice(-2);

              var now_year = ("0000" + (now_date.getFullYear().toString())).slice(-4);
              var now_month = ("00" + ((now_date.getMonth() + 1).toString())).slice(-2);
              var now_day = ("00" + ((now_date.getDate()).toString())).slice(-2);
              var now_hours = ("00" + (now_date.getHours().toString())).slice(-2);
              var now_minutes = ("00" + (now_date.getMinutes().toString())).slice(-2);
              var now_seconds = ("00" + (now_date.getSeconds().toString())).slice(-2);

              // Since some calendars don't add 0 second events, we need to remove time if there is none...
              var start_time = '';
              var end_time = '';
              if (start_hours + start_minutes + start_seconds + end_hours + end_minutes + end_seconds != 0) {
                start_time = 'T' + start_hours + start_minutes + start_seconds;
                end_time = 'T' + end_hours + end_minutes + end_seconds;
              }
              var now_time = 'T' + now_hours + now_minutes + now_seconds;

              var start = start_year + start_month + start_day + start_time;
              var end = end_year + end_month + end_day + end_time;
              var now = now_year + now_month + now_day + now_time;

              // recurrence rrule vars
              var rruleString;
              if (rrule) {
                if (rrule.rrule) {
                  rruleString = rrule.rrule;
                } else {
                  rruleString = 'rrule:FREQ=' + rrule.freq;

                  if (rrule.until) {
                    var uDate = new Date(Date.parse(rrule.until)).toISOString();
                    rruleString += ';UNTIL=' + uDate.substring(0, uDate.length - 13).replace(/[-]/g, '') + '000000Z';
                  }

                  if (rrule.interval) {
                    rruleString += ';INTERVAL=' + rrule.interval;
                  }

                  if (rrule.count) {
                    rruleString += ';COUNT=' + rrule.count;
                  }

                  if (rrule.byday && rrule.byday.length > 0) {
                    rruleString += ';BYDAY=' + rrule.byday.join(',');
                  }
                }
              }

              var stamp = new Date().toISOString();

              var calendarEvent = [
                'BEGIN:VEVENT',
                'UID:' + calendarEvents.length + "@" + uidDomain,
                'CLASS:PUBLIC',
                'DESCRIPTION:' + description,
                'DTSTAMP;VALUE=DATE-TIME:' + now,
                'DTSTART;VALUE=DATE-TIME:' + start,
                'DTEND;VALUE=DATE-TIME:' + end,
                'LOCATION:' + location,
                'SUMMARY;LANGUAGE=en-us:' + subject,
                'TRANSP:TRANSPARENT',
                'END:VEVENT'
              ];

              if (rruleString) {
                calendarEvent.splice(4, 0, rruleString);
              }

              calendarEvent = calendarEvent.join(SEPARATOR);

              calendarEvents.push(calendarEvent);
              return calendarEvent;
            },

            /**
             * Download calendar using the saveAs function from filesave.js
             * @param  {string} filename Filename
             * @param  {string} ext      Extention
             */
            'download': function(filename, ext) {
              if (calendarEvents.length < 1) {
                return false;
              }

              ext = (typeof ext !== 'undefined') ? ext : '.ics';
              filename = (typeof filename !== 'undefined') ? filename : 'calendar';
              var calendar = calendarStart + SEPARATOR + calendarEvents.join(SEPARATOR) + calendarEnd;

              var blob;
              if (navigator.userAgent.indexOf('MSIE 10') === -1) { // chrome or firefox
                blob = new Blob([calendar]);
              } else { // ie
                var bb = new BlobBuilder();
                bb.append(calendar);
                blob = bb.getBlob('text/x-vCalendar;charset=' + document.characterSet);
              }
              FileSaver.saveAs(blob, filename + ext);
              return calendar;
            },

            /**
             * Build and return the ical contents
             */
            'build': function() {
              if (calendarEvents.length < 1) {
                return false;
              }

              var calendar = calendarStart + SEPARATOR + calendarEvents.join(SEPARATOR) + calendarEnd;

              return calendar;
            }
          };
        }

    }
};
