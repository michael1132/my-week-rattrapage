<?php

/* Slim */
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Container;

/* Config */
use myweek\eloquent\Eloquent;
use myweek\mf\auth\Authentification;

/* JWT */
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;

/* Controllers */
use myweek\controller\AccountController;
use myweek\controller\CategoryController;
use myweek\controller\SubTaskController;
use myweek\controller\TaskController;

/* Model */
use myweek\model\User;

/* Errors / Response */
use myweek\response\Writter;
use myweek\errors\PhpError;

require '../src/vendor/autoload.php';

$apiContainer = new Container(require_once __DIR__ . "/../src/config/config.php");
$app = new App($apiContainer);

Eloquent::startEloquent($apiContainer->settings['dbconf']);

/**
 * @param $req
 * @param $resp
 * @param $next
 * @return mixed|PhpError
 */
$verifToken = function ($req, $resp, $next) {
    if (Authentification::getToken($req)) {
        $token = Authentification::getToken($req);
        $user = User::find($token[1]->userId);
        if (!empty($user)) {
            $key = $user->token;
            try {
                $token = JWT::decode($token[3], $key, ['HS512']);
                return $next($req, $resp);
            } catch (\Exception $e) {
                return PhpError::error($req, $resp, "Identification incorrecte");
            }
        } else {
            return Writter::jsonError($resp, "Utilisateur inconnu", 401);
        }
    } else {
        return Writter::jsonError($resp, "Missing token", 401);
    }

    return $resp;
};


/*-------------CORS------------------*/
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$app->get('/ping[/]', function (Request $request, Response $response, array $args) {
    return Writter::jsonSuccess($response, ['success' => 1]);
});

$app->get('/routes[/]', function (Request $request, Response $response, array $args) {
    $routes = [
      '/signup' => [
          'POST' => [
              'Description'   => 'Créer un compte',
              'Header Params' => 'none',
              'Body Params'   => 'firstName, lastName, username, email, password, verifPassword'
          ],
      ],
      '/login' => [
        'POST' => [
            'Description'   => 'Se connecter',
            'Header Params' => 'Basic Auth',
            'Body Params'   => 'email, password'
        ],
      ],
      '/logout' => [
        'DELETE' => [
            'Description'   => 'Se déconnecter',
            'Header Params' => 'Authorization Bearer {token}',
            'Body Params'   => 'none'
        ],
      ],
      '/users/search' => [
        'GET' => [
            'Description'   => 'Chercher un utilisateur pour le partage de tâches',
            'Header Params' => 'Authorization Bearer {token}',
            'Body Params'   => 'search'
        ],
      ],
      '/tasks' => [
          'GET' => [
              'Description'   => 'Récuperer toutes les tâches',
              'Header Params' => 'Authorization Bearer {token}',
              'Body Params'   => 'none'
          ],
          'POST' => [
              'Description'   => 'Créer une tâche',
              'Header Params' => 'Authorization Bearer {token}',
              'Body Params'   => 'title, description, categoryId, priority, startDate, endDate, subtasks, usersId'
          ],
          'PATCH' => [
              'Description'   => 'Modifier une tâche',
              'Header Params' => 'Authorization Bearer {token}',
              'Body Params'   => 'title, description, categoryId, priority, startDate, endDate, subtasks, usersId'
          ],
          'DELETE' => [
              'Description'   => 'Effacer une tâche',
              'Header Params' => 'Authorization Bearer {token}',
              'Body Params'   => 'id'
          ],
      ],
      '/subtasks' => [
          'GET' => [
                'Description'   => 'Récuperer toutes les sous-tâches',
                'Header Params' => 'Authorization Bearer {token}',
                'Body Params'   => 'none'
            ],
          'POST' => [
                'Description'   => 'Créer une sous-tâche',
                'Header Params' => 'Authorization Bearer {token}',
                'Body Params'   => 'title, taskId'
            ],
          'PATCH' => [
                'Description'   => 'Modifier l\'état d\'une sous-tâche',
                'Header Params' => 'Authorization Bearer {token}',
                'Body Params'   => 'id, check'
            ],
          'DELETE' => [
                'Description'   => 'Effacer une sous-tâche',
                'Header Params' => 'Authorization Bearer {token}',
                'Body Params'   => 'id'
            ],
      ],
      '/profiles/{id}' => [
          'GET' => [
              'Description'   => 'Récuperer l\'utilisateur actuel',
              'Header Params' => 'Authorization Bearer {token}',
              'Body Params'   => 'id'
          ],
          'PATCH' => [
              'Description'   => 'Modifier l\'utilisateur actuel',
              'Header Params' => 'Authorization Bearer {token}',
              'Body Params'   => 'firstName, firstName, username, email, password'
          ],
      ],
      '/categories[/][{id}]' => [
            'GET' => [
                'Description'   => 'Récuperer toutes les categories',
                'Header Params' => 'Authorization Bearer {token}',
                'Body Params'   => 'none'
            ],
            'POST' => [
                'Description'   => 'Créer une categorie',
                'Header Params' => 'Authorization Bearer {token}',
                'Body Params'   => 'name, color'
            ],
            'PATCH' => [
                'Description'   => 'Modifier une tâche',
                'Header Params' => 'Authorization Bearer {token}',
                'Body Params'   => 'id, name, color'
            ],
            'DELETE' => [
                'Description'   => 'Effacer une tâche',
                'Header Params' => 'Authorization Bearer {token}',
                'Body Params'   => 'id'
            ],
        ],
    ];
    return Writter::jsonSuccess($response, $routes);
});


/*-------------Account------------------*/
$app->post('/signup[/]', function (Request $req, Response $resp, array $args) {
    return (new AccountController($this))->create($req, $resp, $args);
});

$app->post('/login[/]', function (Request $req, Response $resp, array $args) {
    return (new AccountController($this))->login($req, $resp, $args);
});

$app->delete('/logout[/]', function (Request $req, Response $resp, array $args) {
    return (new AccountController($this))->logout($req, $resp, $args);
})->add($verifToken);

$app->get('/users/search[/]', function (Request $req, Response $resp, array $args) {
    return (new AccountController($this))->search($req, $resp, $args);
})->add($verifToken);

$app->get('/checkaccess[/]', function (Request $request, Response $response, array $args) {
    return Writter::jsonSuccess($response, array("success" => 1));
})->add($verifToken);

$app->get('/profiles/{id}[/]', function (Request $request, Response $response, array $args) {
    return (new AccountController($this))->index($request, $response, $args);
})->add($verifToken);

$app->patch('/profiles/{id}[/]', function (Request $req, Response $resp, array $args) {
    return (new AccountController($this))->edit($req, $resp, $args);
})->add($verifToken);

$app->get('/postits/{id}[/]', function (Request $request, Response $response, array $args) {
    return (new AccountController($this))->getPostits($request, $response, $args);
})->add($verifToken);

$app->patch('/postits/{id}[/]', function (Request $req, Response $resp, array $args) {
    return (new AccountController($this))->savePostits($req, $resp, $args);
})->add($verifToken);


/*---------------Task------------------*/
$app->get('/tasks[/]', function (Request $request, Response $response, array $args) {
    return (new TaskController($this))->index($request, $response, $args);
})->add($verifToken);

$app->post('/tasks[/]', function (Request $request, Response $response, array $args) {
    return (new TaskController($this))->create($request, $response, $args);
})->add($verifToken);

$app->patch('/tasks/{id}[/]', function (Request $request, Response $response, array $args) {
    return (new TaskController($this))->edit($request, $response, $args);
})->add($verifToken);

$app->delete('/tasks/{id}[/]', function (Request $request, Response $response, array $args) {
    return (new TaskController($this))->delete($request, $response, $args);
})->add($verifToken);



/*-------------SubTask------------------*/
$app->get('/subtasks[/]', function (Request $request, Response $response, array $args) {
    return (new SubTaskController($this))->index($request, $response, $args);
})->add($verifToken);

$app->post('/subtasks[/]', function (Request $request, Response $response, array $args) {
    return (new SubTaskController($this))->create($request, $response, $args);
})->add($verifToken);

$app->patch('/subtasks/{id}[/]', function (Request $request, Response $response, array $args) {
    return (new SubTaskController($this))->edit($request, $response, $args);
})->add($verifToken);

$app->delete('/subtasks/{id}[/]', function (Request $request, Response $response, array $args) {
    return (new SubTaskController($this))->delete($request, $response, $args);
})->add($verifToken);



/*-------------Categories------------------*/
$app->get('/categories[/]', function (Request $request, Response $response, array $args) {
    return (new CategoryController($this))->index($request, $response, $args);
})->add($verifToken);

$app->post('/categories[/]', function (Request $request, Response $response, array $args) {
    return (new CategoryController($this))->create($request, $response, $args);
})->add($verifToken);

$app->patch('/categories/{id}[/]', function (Request $request, Response $response, array $args) {
    return (new CategoryController($this))->edit($request, $response, $args);
})->add($verifToken);

$app->delete('/categories/{id}[/]', function (Request $request, Response $response, array $args) {
    return (new CategoryController($this))->delete($request, $response, $args);
})->add($verifToken);

// Catch-all route to serve a 404 Not Found page if none of the routes match
// NOTE: make sure this route is defined last
$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function ($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});

$app->run();