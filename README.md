# MyWeek est une application web ergonomique et pratique pour la gestion de tâches avec vision à long terme sur les tâches à venir, interaction avec un agenda Caldav et une gestion des priorités et des notifications.

## Backend développé avec Slim, Eloquant, JWT firebase

## Fronted développé avec Vuejs, axios, hooper, ics-to-json, moment, vuetify, vuex, vue-router

### Clonnage du projet git
#### ssh :
> git clone git@bitbucket.org:gestiondetaches/myweek.git
#### https :
> git clone https://baptistedelbary@bitbucket.org/gestiondetaches/myweek.git

### Installation du docker compose
> docker-compose -f docker-compose.yml up

#### Pour les dépendances backend (dossier src)
> composer install

### Initialisation de vuejs (dossier web)
> npm install
> npm install --save-dev  --unsafe-perm node-sass
> npm install --save file-saver
### --------------
> npm run serve

### Acces au service apache
http://api.myweek.local:3030
https://api.myweek.local:4040

### Acces a adminer
http://localhost:5050
